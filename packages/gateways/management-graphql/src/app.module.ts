import {Module} from '@nestjs/common';
import {AppResolver} from './app.resolver';
import {AppService} from './app.service';
import {GraphQLModule} from '@nestjs/graphql';
import {AccountsClientModule} from '@mmga/accounts.client';

@Module({
    imports: [
        AccountsClientModule,
        GraphQLModule.forRoot({
            autoSchemaFile: 'schema.graphql',
            playground: true
        })
    ],
    controllers: [],
    providers: [AppService, AppResolver],
})
export class AppModule {
}
