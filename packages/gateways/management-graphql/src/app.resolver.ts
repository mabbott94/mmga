import { AppService } from './app.service';
import {Resolver, Query} from '@nestjs/graphql'
import {GreetingType} from '@mmga/types';
import {AccountService} from '@mmga/accounts.client';

@Resolver()
export class AppResolver {

  constructor(private readonly service: AccountService) {}

  @Query(returns => GreetingType)
  getHello(): any {
    return this.service.greeting();
  }
}
