import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {AccountsClientModule} from '@mmga/accounts.client';

@Module({
  imports: [AccountsClientModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
