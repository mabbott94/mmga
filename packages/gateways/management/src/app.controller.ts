import { Controller, Get } from '@nestjs/common';
import {AccountService} from '@mmga/accounts.client';


@Controller()
export class AppController {
  constructor(private readonly accountService: AccountService) {}

  @Get()
  getHello() {
    return this.accountService.greeting()
  }
}
