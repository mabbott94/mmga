import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { Client, ClientGrpc } from '@nestjs/microservices';
import { getAccountGRPCConnectionOptions, AccountsService } from '@mmga/interfaces';

@Injectable()
export class AccountService implements OnModuleInit {
    private readonly logger = new Logger(AccountService.name);

    @Client(getAccountGRPCConnectionOptions('../../../../protos/'))
    private client: ClientGrpc;


    private service: AccountsService;

    onModuleInit() {
        this.service = this.client.getService<AccountsService>('AccountService');
    }

    greeting() {
        this.logger.log("greeting request from grpc client");
        return this.service.greeting({});
    }
}
