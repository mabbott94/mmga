import {GreetingResponse} from '@mmga/stubs/packages';

export interface AccountsService {
    greeting(Empty): GreetingResponse;
}
