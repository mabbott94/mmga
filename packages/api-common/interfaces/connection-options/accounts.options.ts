import { Transport, ClientOptions } from "@nestjs/microservices";
import { join } from "path";

// export const AccountGRPCConnectionOptions: ClientOptions = {
//     transport: Transport.GRPC,
//     options: {
//         url: "localhost:5001",
//         package: "accounts",
//         protoPath: join(__dirname, "../../../../protos/accounts.proto"),
//     }
// };

export const getAccountGRPCConnectionOptions = (protoPath: string): ClientOptions => {
    return {
        transport: Transport.GRPC,
        options: {
            url: "localhost:5001",
            package: "accounts",
            protoPath: join(__dirname, `${protoPath}accounts.proto`),
        }
    }
};