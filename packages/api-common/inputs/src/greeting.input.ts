import {Field, InputType} from 'type-graphql';
import {IsDefined} from 'class-validator';

@InputType()
export class GreetingInput {
    @Field()
    @IsDefined()
    greeting: string
}
