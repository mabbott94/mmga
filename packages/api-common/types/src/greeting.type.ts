import {Field, ObjectType} from 'type-graphql';

@ObjectType()
export class GreetingType {
    @Field(type => String)
    greeting: string
}
