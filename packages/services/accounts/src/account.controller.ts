import { Controller, Get, Logger } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';

@Controller()
export class AccountController {
    private readonly logger = new Logger(AccountController.name);
    constructor() { }

    @GrpcMethod('AccountService', 'Greeting')
    greeting() {
        this.logger.log('GRPC method greeting pending');
        return {
            greeting: 'Hello from accounts service'
        }
    }
}
