import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {getAccountGRPCConnectionOptions} from '@mmga/interfaces';
import {Logger} from '@nestjs/common';

const logger = new Logger('Account Service Main');

async function bootstrap() {
    const app = await NestFactory.createMicroservice(
        AppModule,
        getAccountGRPCConnectionOptions('../../../../protos/'),
    );

    logger.log('preparing to listen');

    await app.listenAsync();
}

bootstrap();
